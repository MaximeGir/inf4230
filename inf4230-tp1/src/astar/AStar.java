/* INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package astar;

import java.text.NumberFormat;
import java.util.*;

public class AStar {

    public static List<Action> genererPlan(Monde monde, Etat etatInitial, But but, Heuristique heuristique){
    	double ajust = 0.000001;
        long starttime = System.currentTimeMillis();
        boolean condition = true;
        List<Action> plan = new ArrayList<Action>();
        // À Compléter.
        // Implémentez l'algorithme A* ici.
        
        // Étapes suggérées :
        //  - Restez simple.
        
        //  - Ajoutez : TreeSet<Etat> open, close;
        
        TreeSet<Etat> open = new TreeSet<Etat>(new fComparator());
        TreeSet<Etat> close = new TreeSet<Etat>(new fComparator());
        
        //  - Ajoutez etatInitial dans open.
        if(etatInitial.parent != null){
        	etatInitial.g = etatInitial.actionDepuisParent.cout;
        }
        etatInitial.h = heuristique.estimerCoutRestant(etatInitial, but);
        etatInitial.f = etatInitial.g + etatInitial.h;
        open.add(etatInitial);
        
        //  - Numérotez les itérations.
        //  - Pour chaque itération :
        //  --  Affichez le numéro d'itération.
        //  --  Faites une boucles qui itère tous les états e dans open pour trouver celui avec e.f minimal.
        
        Etat e = etatInitial;
        int etatsGeneres = 0;
        int etatsVisites = 0;
        double cout = 0;
       
        while(condition){
        	/*if(iteration > 50){
        		condition = false;
        	}*/
        	if(open.isEmpty()){
        		//System.out.print("Open est vide. \n");  
        		condition = false;
        		break;
        	}    

        	etatsVisites++;
        	//Affiche le numero d'iteration.
        	//System.out.print("-------------------------------------------------");   
            //System.out.print("iteration " + iteration + "\n");   
            
            //enlever e de open et leajouter dans closed
            e = open.first();
            //System.out.print("enleve de open: " + open.first().toString() + "\n");
            //System.out.print("size de open av enlvement: " + open.size() + "\n");
            open.pollFirst();
            //System.out.print("size de open pres enlvement: " + open.size() + "\n");
            //System.out.print("first de  open:" + e.toString() + "\n"); 
            //System.out.print("ajoute a close: " + e.toString() + "\n");
            close.add(e);   
            
            //Affiche le nom du noed selectionne.
            //System.out.print("etat selectionne: " + e.toString() + "\n");
            //Affiche le f de l'etat selectionne.
            //System.out.print("f de l'etat selectionne: " + e.f + "\n");
            
            //  --  Vérifiez si l'état e satisfait le but. 
            //  ---   Si oui, sortez du while.
            if(but.butSatisfait(e)){
            	//System.out.print("But satisfait \n");
            	while(e.actionDepuisParent != null){
            		plan.add(0, (e.actionDepuisParent));
            		cout += e.actionDepuisParent.cout;
            		e = e.parent;
            	}
            	condition = false;
            	break;
        	}
            //  ---   Une autre boucle remonte les pointeurs parents.
            //  --  Générez les successeurs de e.
            //List<Action> successeurs = monde.getActions(e);
            //Iterator<Action> iterSuccessurs = successeurs.iterator();
            //int iterationSucesseurs = 0;
            //  --  Pour chaque état successeur s de e:

            for(Action s : monde.getActions(e)){
            	Etat es = monde.executer(e, s); 	
            	etatsGeneres++;
            	es.parent = e;
            	es.actionDepuisParent = s;
                //  ---   Calculez s.etat.g = e.g + s.cout.
            	es.g = es.actionDepuisParent.cout + e.g;
            	//System.out.print("g pour sucesseur " + es.toString()+ " " + es.g + "\n");  
            	es.h = heuristique.estimerCoutRestant(es, but);
            	//System.out.print("h:" + es.h + "\n");  
            	es.f = es.g + es.h;   
            	//System.out.print("f:" + es.f + "\n");  
 
                //  ---   Vérifiez si s.etat est dans closed.
           	    //  ---   Vérifiez si s.etat existe dans open.
	            //  ----    Si s.etat est déjà dans open, vérifiez son .f.
            	//  ---   Ajoutez s.etat dans open si nécessaire.
            	Boolean contenu = false;
            	Boolean dansOpen = false;
            	Boolean dansClose = false;

            	Iterator<Etat> iterOpen = open.iterator();   
            	//System.out.println("avant iter open");
            	//System.out.println("iteropen has next:"+iterOpen.hasNext());
            	//System.out.println("dansOpen:"+dansOpen);
            	while((iterOpen.hasNext())&&(dansOpen == false)){
            		Etat n3 = iterOpen.next();
            		//System.out.print("test open ev n3: " + n3.toString() + "\n");
            		//System.out.print("test open ev es: " + es.toString() + "\n");
	           		if((n3.toString()).equals(es.toString())){
		           		//System.out.print("Open contient le successeur e	quivalent: " + es.toString() + "\n");
		           		contenu = true;
		           		if(es.f < n3.f){
			           		open.remove(n3);
			           		open.add(es);
			           		//System.out.print("Le successeur remplace l'equivalent. \n");
		           		}
		           		dansOpen = true;
		           	}           		
            	}
            	Iterator<Etat> iterClose = close.iterator();
            	//System.out.println("avant iter close");
            	//System.out.println("iterclose has next:" + iterClose.hasNext());
            	//System.out.println("dansClose:"+dansClose);
            	while((iterClose.hasNext())&&(dansClose == false)){
            		Etat n3 = iterClose.next();
            		//System.out.print("test close ev n3: " + n3.toString() + "\n");
            		//System.out.print("test close ev es: " + es.toString() + "\n");
	           		if((n3.toString()).equals(es.toString())){
	           			//System.out.print("Close contient le successeur e	quivalent: " + es.toString() + "\n");
		           		contenu = true;
		           		//System.out.print("es.f: " + es.f + "n3.f" + n3.f +"\n");
		           		if(es.f < n3.f){
			           		close.remove(n3);
			           		open.add(es);
			           		//System.out.print("Le successeur remplace l'equivalent. \n");
		           		}
		           		dansClose = true;
		           	} 		
            	}
	           	if(contenu == false){
	           		//System.out.print("open size precedent: " + open.size() + "\n");
	           		int size1 = 0;
	           		size1 =  open.size();
	           		if(open.size() == size1){
	           			es.f += ajust;
	           			open.add(es);
	           		}else{
	           			open.add(es);
	           		}
	           		//System.out.print("open size: " + open.size() + "\n");
	           		//System.out.print("ajoute a open:" + es.toString() + "\n"); 
	           		//System.out.print("f: " + open.first().f + "\n");
	            }
	           	ajust += 0.000001;
            }//end while(iterSuccessurs.hasNext())  
  
        }//end while(condition)
   
        // Un plan est une séquence (liste) d'actions.


        long lastDuration = System.currentTimeMillis() - starttime;
        // Les lignes écrites débutant par un dièse '#' seront ignorées par le valideur de solution.
        System.out.println("# Nombre d'états générés : " + etatsGeneres);
        System.out.println("# Nombre d'états visités : " + etatsVisites);
        System.out.println("# Durée : " + lastDuration + " ms");
        System.out.println("# Coût : "+ nf.format(cout));
        
        return plan;
    }
    
    static final NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
    static {
        nf.setMaximumFractionDigits(1);
    }
}

