package astar;

import java.util.Comparator;

public class fComparator implements Comparator<Etat> {

    @Override
    public int compare(Etat e1, Etat e2) {
    	return Double.compare(e1.f, e2.f);
   	}
}
