package astar;

import java.util.Comparator;

public class EtatComparator implements Comparator<Etat>{
	@Override
	public int compare(Etat o1, Etat o2) {
	    if(o1.f < o2.f) return -1;
	    if(o1.f > o2.f) return 1;
		return 0;
	}
  
	
}
