/* 
 * INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */

package sokoban;


import java.util.ArrayList;
import java.util.TreeMap;

import astar.Etat;

/**
 * Représente un état d'un monde du jeu Sokoban.
 */

public class EtatSokoban extends astar.Etat{
	
    public Grille jeu;
    
    public EtatSokoban(Grille jeu){
       this.jeu = jeu;
    }
    public EtatSokoban() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public EtatSokoban clone(){
		ArrayList<PositionGrille> buts = new ArrayList<>();
		ArrayList<PositionGrille> bonhomme = new ArrayList<>();
		ArrayList<PositionGrille> blocs = new ArrayList<>();
		
		buts.addAll(this.jeu.positions.get("positionButs"));
		blocs.addAll(this.jeu.positions.get("positionBlocs"));
		bonhomme.addAll(this.jeu.positions.get("positionBonhomme"));
		
		EtatSokoban etatCloner = new EtatSokoban();
		etatCloner.jeu = new Grille();
		etatCloner.jeu.grille = new char[this.jeu.grille.length][];
		
		for(int i = 0; i < this.jeu.grille.length; i++){
			 etatCloner.jeu.grille[i] = this.jeu.grille[i].clone();
		}
		
		etatCloner.jeu.positions = new TreeMap<String, ArrayList<PositionGrille>>();
		etatCloner.jeu.positions.put("positionButs", buts);
		etatCloner.jeu.positions.put("positionBlocs", blocs);
		etatCloner.jeu.positions.put("positionBonhomme", bonhomme);
		return etatCloner;
	}
	
    @Override
    public int compareTo(Etat o) {
        EtatSokoban es = (EtatSokoban) o;
        return Double.compare(es.f, f);
    }
    
    @Override
    public String toString() {
    	String output = "";
    	
    	for(int i = 0; i < this.jeu.grille.length; i++){
   	       for(int j = 0; j < this.jeu.grille[i].length; j++){
   	    	output = output + (this.jeu.grille[i][j]);
   	       }
   	    }
    	return output;
    }

    
}
