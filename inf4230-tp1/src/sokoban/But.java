/* 
 * INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package sokoban;

/**
 * Représente un but.
 */

public class But implements astar.But, astar.Heuristique {
	EtatSokoban etatInitial;
	public But(EtatSokoban etatInitial){
		this.etatInitial = etatInitial.clone();

		for(int i = 0; i < this.etatInitial.jeu.grille.length; i++)
			for(int j = 0; j< this.etatInitial.jeu.grille[i].length; j++)
				if(this.etatInitial.jeu.grille[i][j] == '$') this.etatInitial.jeu.grille[i][j] = ' ';

		for(PositionGrille position: this.etatInitial.jeu.positions.get("positionButs")){
			this.etatInitial.jeu.grille[position.getX()][position.getY()] = '$';
			int index = this.etatInitial.jeu.positions.get("positionButs").indexOf(position);
			this.etatInitial.jeu.positions.get("positionBlocs").set(index, new PositionGrille(position.getX(),position.getY()));
		}
	}

	@Override
	public boolean butSatisfait(astar.Etat e) {
		EtatSokoban es = (EtatSokoban) e;
		for(PositionGrille pg : es.jeu.positions.get("positionButs")){
			if(es.jeu.grille[pg.getX()][pg.getY()] != '$'){
				return false;
			}
		}  
		return true;
	}

	@Override
	public double estimerCoutRestant(astar.Etat e, astar.But b) {
		EtatSokoban etat = (EtatSokoban) e;
		double distance = 0.0;
		PositionGrille positionBonhomme = etat.jeu.positions.get("positionBonhomme").get(0);
		//pour chaque bloc
		for(PositionGrille pbloc: etat.jeu.positions.get("positionBlocs")){
			//calcul de la moyenne de la distance vers un but
			for(PositionGrille pbut: etat.jeu.positions.get("positionButs")){
				distance += pbloc.calculerDistanceManhattan(pbut);
			}
			distance = distance/(etat.jeu.positions.get("positionBlocs").size())*0.5;
		}
		for(PositionGrille pbloc: etat.jeu.positions.get("positionBlocs")){
			distance += positionBonhomme.calculerDistanceManhattan(pbloc);
		}
		return distance;
		
	}

	public double butLePlusPres(EtatSokoban etat, PositionGrille positionBox){
		double min = positionBox.calculerDistanceManhattan(etat.jeu.positions.get("positionButs").get(0));
		for(PositionGrille pg: etat.jeu.positions.get("positionButs")){
			min = pg.calculerDistanceManhattan(positionBox) < min ? pg.calculerDistanceManhattan(positionBox) : min;
		}
		return min;
	}

}
