/* INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package sokoban;

import java.util.*;

/**
 * Dans le jeu de sokoban, le «Monde» est une «Grille».
 */
public class Grille implements astar.Monde, astar.But {
	
	int OFFSET_NORD = -1;
	int OFFSET_SUD = 1;
	int OFFSET_OUEST = -1;
	int OFFSET_EST = 1;
	
	char[][] grille;
	Map<String, ArrayList<PositionGrille>> positions;

	public Grille(ArrayList<String> lignes) {
		int i = 0;
		this.grille = new char[lignes.size()][];
		for (String line : lignes) {
			this.grille[i++] = line.toCharArray();
		}
		positions = new TreeMap<>();
	}
	
	public Grille(){
		
	}
	
	@Override
	public List<astar.Action> getActions(astar.Etat e) {
		
		EtatSokoban etat = (EtatSokoban) e;
		ArrayList<astar.Action> listeAction = new ArrayList<>();
		Map<String, ArrayList<PositionGrille>> positions = etat.jeu.positions;

		PositionGrille bonhomme = positions.get("positionBonhomme").get(0);
		int[][] positionAdjacentes = new int[4][2];
		
		//System.out.println("position bonhomme:" + bonhomme.getX() + ":" + bonhomme.getY());
		//System.out.println("case au sud:" + this.grille[bonhomme.getX() + 1][bonhomme.getY()]);

		positionAdjacentes[0][0] = bonhomme.getX() + OFFSET_SUD;// SUD
		positionAdjacentes[0][1] = bonhomme.getY();

		positionAdjacentes[1][0] = bonhomme.getX() + OFFSET_NORD;// NORD
		positionAdjacentes[1][1] = bonhomme.getY();

		positionAdjacentes[2][0] = bonhomme.getX(); // EST
		positionAdjacentes[2][1] = bonhomme.getY() + OFFSET_EST;

		positionAdjacentes[3][0] = bonhomme.getX();// OUEST
		positionAdjacentes[3][1] = bonhomme.getY() + OFFSET_OUEST;

		for (int i = 0; i < positionAdjacentes.length; i++) {
			boolean ajoutable = true;
			int alpha_position = positionAdjacentes[i][0];
			int beta_position = positionAdjacentes[i][1];
            
			if (etat.jeu.grille[alpha_position][beta_position] == '#') {
				ajoutable = false;
			}
			//System.out.println("position bonhomme:" + bonhomme.getX() + ":" + bonhomme.getY());
			//System.out.println("verification $ pour case: " + i);
			//System.out.println("case au sud:" + etat.jeu.grille[bonhomme.getX() + 1][bonhomme.getY()]);
			//System.out.println("case alpha beta:" + etat.jeu.grille[alpha_position][beta_position] + "pour i: " + i);
			if (etat.jeu.grille[alpha_position][beta_position] == '$') {
				switch (i) {
				//verification sud
				case 0:
					//System.out.println("verification $ sud");
					if ((etat.jeu.grille[alpha_position + 1][beta_position] == '#')
							|| (etat.jeu.grille[alpha_position + 1][beta_position] == '$')) {
						ajoutable = false;
					}else if(etat.jeu.grille[alpha_position + 1][beta_position] != '.'){
						if(verifieCoins(etat,alpha_position,beta_position,i) == false){
							ajoutable = false;
						}
					}
					   break;
				//verification nord
				case 1:
					//System.out.println("verification $ nord");
					if ((etat.jeu.grille[alpha_position - 1][beta_position] == '#')
							|| (etat.jeu.grille[alpha_position - 1][beta_position] == '$')) {
						ajoutable = false;
					}else if(etat.jeu.grille[alpha_position - 1][beta_position] != '.'){
						if(verifieCoins(etat,alpha_position,beta_position,i) == false){
							ajoutable = false;
						}
					}
					break;
				//verification est
				case 2:
					//System.out.println("verification $ est");
					if ((etat.jeu.grille[alpha_position][beta_position + 1] == '#')
							|| (etat.jeu.grille[alpha_position][beta_position + 1] == '$')) {
						ajoutable = false;						
					}else if(etat.jeu.grille[alpha_position][beta_position + 1] != '.'){
						if(verifieCoins(etat,alpha_position,beta_position,i) == false){
							ajoutable = false;
						}
					}
					break;
					//verification ouest
				case 3:
					//System.out.println("verification $ ouest");
					if ((etat.jeu.grille[alpha_position][beta_position - 1] == '#')
							|| (etat.jeu.grille[alpha_position][beta_position - 1] == '$')) {
							ajoutable = false;
					}else if(etat.jeu.grille[alpha_position][beta_position - 1] != '.'){
						if(verifieCoins(etat,alpha_position,beta_position,i) == false){
							ajoutable = false;
						}
					}
					break;
				}	
			}
            
			if(ajoutable == true){    
				switch (i) {
				case 0:
					listeAction.add(new ActionDeplacement("S"));
					break;
				case 1:
					listeAction.add(new ActionDeplacement("N"));
					break;
				case 2:
					listeAction.add(new ActionDeplacement("E"));
					break;
				case 3:
					listeAction.add(new ActionDeplacement("W"));
					break;
				default:
					break;
				}
			}
		}
		//System.out.println("DIRECTION POSSIBLE : "+listeAction);
		return listeAction;
	}	

	@Override
	public astar.Etat executer(astar.Etat e, astar.Action a) {
		
		ActionDeplacement action = (ActionDeplacement) a;
		EtatSokoban etat = (EtatSokoban) e;
		Object objetSokoban = etat.clone();
		EtatSokoban etatSuccesseur = (EtatSokoban) objetSokoban;
	    
		PositionGrille positionB = etatSuccesseur.jeu.positions.get("positionBonhomme").get(0);
        char direction = action.nom.charAt(0);
        
        switch(direction){
		case 'N':
			//on met un vide ou une cible dans la grille ou �tait le bonhomme
			etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = ' ';
			for(PositionGrille but: etatSuccesseur.jeu.positions.get("positionButs")){
				if((but.getX() == positionB.getX())&&(but.getY() == positionB.getY())){
					etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = '.';
				}
			}
			//on monte le bonhomme dans la grille
			etatSuccesseur.jeu.grille[positionB.getX()+OFFSET_NORD][positionB.getY()] = '@';
			//on monte le bonhomme dans le map
			etatSuccesseur.jeu.positions.get("positionBonhomme").set(0,new PositionGrille(positionB.getX()+OFFSET_NORD,positionB.getY()));
			//pour tout les blocs
			for(PositionGrille pgb: etatSuccesseur.jeu.positions.get("positionBlocs")){
				//si il y a le bonhomme a la position du bloc
				if(pgb.equals(etatSuccesseur.jeu.positions.get("positionBonhomme").get(0))){
					//index de la position du bloc
					int index = etatSuccesseur.jeu.positions.get("positionBlocs").indexOf(pgb);
					//on monte le bloc dans le map
					etatSuccesseur.jeu.positions.get("positionBlocs").set(index,new PositionGrille(pgb.getX()+OFFSET_NORD,pgb.getY()));
					//on monte le bloc dans le grille;
					etatSuccesseur.jeu.grille[pgb.getX()+OFFSET_NORD][pgb.getY()] = '$';

				}
			}
			break;
		case 'S':
		//on met un vide ou une cible dans la grille ou �tait le bonhomme
		etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = ' ';
		for(PositionGrille but: etatSuccesseur.jeu.positions.get("positionButs")){
			if((but.getX() == positionB.getX())&&(but.getY() == positionB.getY())){
				etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = '.';
			}
		}
		etatSuccesseur.jeu.grille[positionB.getX()+OFFSET_SUD][positionB.getY()] = '@';
		etatSuccesseur.jeu.positions.get("positionBonhomme").set(0,new PositionGrille(positionB.getX()+OFFSET_SUD,positionB.getY()));
		for(PositionGrille pgb: etatSuccesseur.jeu.positions.get("positionBlocs")){
			if(pgb.equals(etatSuccesseur.jeu.positions.get("positionBonhomme").get(0))){
				int index = etatSuccesseur.jeu.positions.get("positionBlocs").indexOf(pgb);
				etatSuccesseur.jeu.positions.get("positionBlocs").set(index,new PositionGrille(pgb.getX()+OFFSET_SUD,pgb.getY()));
					etatSuccesseur.jeu.grille[pgb.getX()+OFFSET_SUD][pgb.getY()] = '$';
			}
		}
		break;
		case 'E': 
			//on met un vide ou une cible dans la grille ou �tait le bonhomme
			etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = ' ';
			for(PositionGrille but: etatSuccesseur.jeu.positions.get("positionButs")){
				if((but.getX() == positionB.getX())&&(but.getY() == positionB.getY())){
					etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = '.';
				}
			}
			etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()+OFFSET_EST] = '@';
			etatSuccesseur.jeu.positions.get("positionBonhomme").set(0,new PositionGrille(positionB.getX(),positionB.getY()+OFFSET_EST));
			for(PositionGrille pgb: etatSuccesseur.jeu.positions.get("positionBlocs")){
				if(pgb.equals(etatSuccesseur.jeu.positions.get("positionBonhomme").get(0))){
					int index = etatSuccesseur.jeu.positions.get("positionBlocs").indexOf(pgb);
					etatSuccesseur.jeu.positions.get("positionBlocs").set(index,new PositionGrille(pgb.getX(),pgb.getY()+OFFSET_EST));
						etatSuccesseur.jeu.grille[pgb.getX()][pgb.getY()+OFFSET_EST] = '$';
				}
			}
			break;
		case 'W':
			//on met un vide ou une cible dans la grille ou �tait le bonhomme
			etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = ' ';
			for(PositionGrille but: etatSuccesseur.jeu.positions.get("positionButs")){
				if((but.getX() == positionB.getX())&&(but.getY() == positionB.getY())){
					etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()] = '.';
				}
			}
			etatSuccesseur.jeu.grille[positionB.getX()][positionB.getY()+OFFSET_OUEST] = '@';
			etatSuccesseur.jeu.positions.get("positionBonhomme").set(0,new PositionGrille(positionB.getX(),positionB.getY()+OFFSET_OUEST));
			for(PositionGrille pgb: etatSuccesseur.jeu.positions.get("positionBlocs")){
				if(pgb.equals(etatSuccesseur.jeu.positions.get("positionBonhomme").get(0))){
					int index = etatSuccesseur.jeu.positions.get("positionBlocs").indexOf(pgb);
					etatSuccesseur.jeu.positions.get("positionBlocs").set(index,new PositionGrille(pgb.getX(),pgb.getY()+OFFSET_OUEST));
						etatSuccesseur.jeu.grille[pgb.getX()][pgb.getY()+OFFSET_OUEST] = '$';	
				}
			}
			break;
		}
       	
        //System.out.println("AFFICHAGE ANCIENNE GRILLE ");
        //Grille.afficher(etat.jeu);
        //System.out.println("AFFICHAGE NOUVELLE GRILLE ");
		//Grille.afficher(etatSuccesseur.jeu);
		//System.out.println("Retourne etat successeur");
		return etatSuccesseur;
	}

	/** Retourne */
	
	@Override
	public boolean butSatisfait(astar.Etat e) {
		return false;
	}
	
	public static void afficher(Grille g){
	 for(int i = 0; i < g.grille.length; i++){
		 System.out.println();
	     for(int j = 0; j < g.grille[i].length; j++)
			 System.out.print(g.grille[i][j]);
	  }
	 System.out.println();
	 }
	//v�rifications des "coins" de murs
	public static boolean verifieCoins(EtatSokoban e, int alpha_position, int beta_position,int cas){
		boolean ajoutable = true;
		EtatSokoban etat = e;
		if(cas == 1){
			if((etat.jeu.grille[alpha_position-2][beta_position] == '#')||(etat.jeu.grille[alpha_position-2][beta_position] == '$')){
				if((etat.jeu.grille[alpha_position-1][beta_position+1] == '#')||(etat.jeu.grille[alpha_position-1][beta_position+1] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!NORD");
				}
				
			}
			if((etat.jeu.grille[alpha_position-1][beta_position-1] == '#')||(etat.jeu.grille[alpha_position-1][beta_position-1] == '$')){
				if((etat.jeu.grille[alpha_position-2][beta_position] == '#')||(etat.jeu.grille[alpha_position-2][beta_position] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!NORD");
				}
				
			}
		}
		if(cas == 0){
			if((etat.jeu.grille[alpha_position+2][beta_position] == '#')||(etat.jeu.grille[alpha_position+2][beta_position] == '$')){
				if((etat.jeu.grille[alpha_position+1][beta_position+1] == '#')||(etat.jeu.grille[alpha_position+1][beta_position+1] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!SUD");
				}
				
			}
			if((etat.jeu.grille[alpha_position+2][beta_position] == '#')||(etat.jeu.grille[alpha_position+2][beta_position] == '$')){
				if((etat.jeu.grille[alpha_position+1][beta_position-1] == '#')||(etat.jeu.grille[alpha_position+1][beta_position-1] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!SUD");
				}
				
			}
		}
		if(cas == 2){
			if((etat.jeu.grille[alpha_position][beta_position+2] == '#')||(etat.jeu.grille[alpha_position][beta_position+2] == '$')){
				if((etat.jeu.grille[alpha_position-1][beta_position+1] == '#')||(etat.jeu.grille[alpha_position-1][beta_position+1] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!EST1");
				}
				
			}
			if((etat.jeu.grille[alpha_position][beta_position+2] == '#')||(etat.jeu.grille[alpha_position-1][beta_position+2] == '$')){
				if((etat.jeu.grille[alpha_position+1][beta_position+1] == '#')||(etat.jeu.grille[alpha_position+1][beta_position+1] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!EST2");
				}
				
			}
		}
		if(cas == 3){
			if((etat.jeu.grille[alpha_position][beta_position-2] == '#')||(etat.jeu.grille[alpha_position][beta_position-2] == '$')){
				if((etat.jeu.grille[alpha_position-1][beta_position-1] == '#')||(etat.jeu.grille[alpha_position-1][beta_position-1] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!OUEST");
				}
				
			}
			if((etat.jeu.grille[alpha_position][beta_position-2] == '#')||(etat.jeu.grille[alpha_position][beta_position-2] == '$')){
				if((etat.jeu.grille[alpha_position+1][beta_position-1] == '#')||(etat.jeu.grille[alpha_position+1][beta_position-1] == '$')){
					ajoutable = false;
					//System.out.println("TEST!!!!!!OUEST");
				}
				
			}
		}
		return ajoutable;
	}

}
