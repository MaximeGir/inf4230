/* 
 * INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package sokoban;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *  Représente un problème chargé d'un fichier test sokoban??.txt.
 */
public class Probleme {
    
	public Grille grille;
    public EtatSokoban etatInitial;
    public But but;
    
    public static Probleme charger(BufferedReader br) throws IOException{
        // Lire les lignes dans fichiers
        ArrayList<String> lignes = new ArrayList<String>();
        String ligne;
        while((ligne = br.readLine())!=null && !ligne.isEmpty()){
            lignes.add(ligne);
            
        }
        
        Probleme probleme = new Probleme(); 
        // Traiter les lignes lue. La grille a lignes.size() lignes.
        probleme.grille = new Grille(lignes);
        probleme.etatInitial = new EtatSokoban(probleme.grille);
        probleme.etatInitial = trouverPositions(probleme);
        probleme.but = new But(probleme.etatInitial);
        //System.out.println("affichage grille but");
        //Grille.afficher(probleme.but.etatInitial.jeu);
        return probleme;
    }
    
    public static EtatSokoban trouverPositions(Probleme probleme){
    	ArrayList<PositionGrille> listeBlocs = new ArrayList<>();
    	ArrayList<PositionGrille> listeButs = new ArrayList<>();
    	ArrayList<PositionGrille> listeBonhomme = new ArrayList<>();
    	
        int i = 0, j = 0, coordonneeX = 0, coordoneeY = 0;
        for(char[] c : probleme.grille.grille){
        	i++;
        	j = 0;
        	for(char e : c){
        	   j++;
        	   if(e == '@') {
        		   coordonneeX = i; 
        		   coordoneeY = j;
        		   PositionGrille positionBonhomme = new PositionGrille(coordonneeX-1, coordoneeY-1);
        		   listeBonhomme.add(positionBonhomme);
        	       probleme.etatInitial.jeu.positions.put("positionBonhomme", listeBonhomme);
        	   }
        	   
        	   else if(e == '$') {
        		   coordonneeX = i; 
        		   coordoneeY = j;
        		   PositionGrille positionBlocs = new PositionGrille(coordonneeX-1, coordoneeY-1);
        		   listeBlocs.add(positionBlocs);
        	       probleme.etatInitial.jeu.positions.put("positionBlocs", listeBlocs);
        	   }
        	   
        	   else if(e == '.') {
        		   coordonneeX = i; 
        		   coordoneeY = j;
        		   PositionGrille positionButs = new PositionGrille(coordonneeX-1, coordoneeY-1);
        		   listeButs.add(positionButs);
        	       probleme.etatInitial.jeu.positions.put("positionButs", listeButs);
        	   }
        	}
        }

        return probleme.etatInitial;
    }
}
