package sokoban;

public class PositionGrille {
	
	private int X;
	private int Y;
	
	public PositionGrille(int X, int Y){
		this.X = X;
		this.Y = Y;
	}

	public int getY(){ return this.Y; }
	public int getX(){ return this.X; }
	
	public boolean equals(PositionGrille position){
	   return (this.X == position.X && this.Y == position.Y);
	}
	
	public PositionGrille updatePosition(int X, int Y){
		this.X = X;
		this.Y = Y;
		return this;
	}
	
	public double calculerDistanceManhattan(PositionGrille position){
		return ( Math.abs(this.getX() - position.getX()) + Math.abs(this.getY() - position.getY()));
	}
}
