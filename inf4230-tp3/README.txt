Instructions pour démarrer le jeu avec le joueur artificiel:

Paramètres de jeu:
Avant de débuter une partie, appuyez sur les touches suivantes pour activer les options correspondantes:

I- Activer le joueur artificiel.

Options pour le joueur artificiel:
1- Mode survie (Le joueur AI (intelligence artificielle) mets claire des lignes le plus souvent possible)
2- Mode Multi (Donne un bonus au joueur ai pour clairer plusieurs lignes à la fois)
3- Mode Tetris (Donne un bonus au joueur ai pour placer les blocs de facon a faire des tetris, soit clairer 4 lignes à la fois.)

Vitesse de déroulement du jeu (si le joueur AI est activé):
9- Les blocs tombent lentement.
0- Les blocs tombent instantanément.

Appuyez ensuite sur la touche retour pour débuter la partie.

------------------------------------------------------------------------------------------

Classes de code provenant de tierces parties:
Auteur: Brendan Jones
Source: https://github.com/PSNB92/Tetris

Tetris.BoardPanel
Tetris.Clock
Tetris.SidePanel
Tetris.java
Tetris.TileType

------------------------------------------------------------------------------------------

CROCODILE TEAM:
Steve Boucher		BOUS30097608	steve.boucher@live.ca
Maxime girard		GIRM30058500	girard.maxime.3@courrier.uqam.ca
Mathieu Tanguay		TANM28028902	mathieu.tanguay871@gmail.com
Dominic Turgeon		TURD14058501	turgeon.dominic@courrier.uqam.ca