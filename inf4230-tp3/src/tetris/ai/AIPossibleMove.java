/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris.ai;

/**
 *
 * @author Steve
 */
public enum AIPossibleMove {
    DROP, LEFT, RIGHT, ANTICLOCKWISE, CLOCKWISE
}
