package tetris.ai;

/*
 * INF4230 - Intelligence artificielle
 * 
 * L'equipe Crocodile - Steve Boucher & friends
 * 
 * AIBoardState - Represente la version AI du BoardPanel, sauf que cette classe-ci est utilise pour 'tester' des etats
 * de jeu et retourne la valeur des etats qui seront calcule.
 * 
 */
public class AIBoardState {

    private AIController game; // Le pont vers le jeu de tetris
    final public AIAction actionFromParent;
    final public AIBoardState etatPrecedent;

    /**
     * Nombre de colonne & ligne - base sur BoardPanel
     * le nombre de ligne inclus 2 invisibles utilise dans BoardPanel
     */
    public static final int COL_COUNT = 10;
    public static final int ROW_COUNT = 22;


    /**
     * Constante "poids" utilise pour calculer la valeur d'un etat
     */
    static final int CLEARED_LINES_WEIGHT = 10; // Le nombre de lignes complete dans l'etat courant
    static final int LOCKED_PIECE_HEIGHT = -12; // La hauteur de la piece qui a ete mis dans l'etat
    static final int NUMBER_OF_WELL_CELL_WEIGHT = -15; // Le nombre de 'puit' (trou de 1 ou + blocs, mais ouvert)
    static final int NUMBER_OF_CLOSED_HOLES_WEIGHT = -80; // Le nombre de 'trou' ferme
    static final int NUMBER_OF_ROW_CHANGES = -30; // Le nombre de shift vide-plein dans une rangee
    static final int TETRIS_WELL_HEIGHT = 10;	  // Un puits tetris est un puit unique partant du bas. Il ne doit pas y avoir de puits ou de trous sur les autres colonnes.



    /**
     * Les tiles - contrairement a BoardPanel, on a juste besoin de savoir s'il sont occupe ou pas.
     *             la coordonnee 0,0 est en haut a gauche dans le tableau et les 2 premiere ligne
     *             sont invisible, ils sont le trigger d'une game over.
     */
    private boolean[][] aiTiles;

    /**
     * variables d'etats - utilise pour definir la valeur de l'etat
     */
    private int numberOfClearedLines; //nombre de lignes complete
    private int newAddedPieceHeight; //la hauteur du nouveau bloc lorsque celui-ci est place
    private int numberOfHoles; //le nombre de trous
    private int numberOfWells; //le nombre de puits
    private int numberOfChangesInsideRows; //le nombre de changements a l'interieur d'une rangee
    private int tetrisWellHeight; //la hauteur du "puits tetris"

    private int stateValue; //la valeur de l'etat

    /**
     * Constructeur
     * @param game 
     */
    public AIBoardState(AIController game) {
        this.game = game;
        this.actionFromParent = null;
        this.etatPrecedent = null;

        aiTiles = new boolean[ROW_COUNT][COL_COUNT];

        numberOfClearedLines = 0;
        newAddedPieceHeight = 0;
        numberOfHoles = 0;
        numberOfChangesInsideRows = 0;
        numberOfWells = 0;
        tetrisWellHeight = 0;

        stateValue = 0;
    }

    
    /**
     * Copy Constructeur
     * @param copy 
     */
    private AIBoardState(AIBoardState copy, AIAction action) {
        this.game = copy.game;
        this.etatPrecedent = copy;
        this.actionFromParent = action.clone();

        this.aiTiles = new boolean[ROW_COUNT][COL_COUNT];

        this.numberOfClearedLines = 0;
        this.newAddedPieceHeight = 0;
        this.numberOfHoles = 0;
        this.numberOfChangesInsideRows = 0;
        this.numberOfWells = 0;
        this.tetrisWellHeight = 0;
        
        this.stateValue = copy.stateValue;

        for (int x = 0; x < ROW_COUNT; x++) {
            System.arraycopy(copy.aiTiles[x], 0, aiTiles[x], 0, COL_COUNT);
        }
    }



    /**
     *  Retourne la value de l'etat actuel du ai board state - Lors du premier tour.
     *  L'idee est de calcule les valeurs qui sont utilise uniquement qu'au premier tour
     */
    int getFirstBlockStateValue() {

        this.stateValue = 0;
        this.stateValue = this.newAddedPieceHeight * LOCKED_PIECE_HEIGHT;

        if (game.getModeAI() == 1)
            this.stateValue += this.numberOfClearedLines * CLEARED_LINES_WEIGHT;
        else
            this.stateValue += Math.pow(CLEARED_LINES_WEIGHT, this.numberOfClearedLines);

        return stateValue;
    }


    /**
     *  Retourne la value de l'etat actuel du ai board state - � partir de la deuxi�me pi�ce.
     */
    int getSecondBlocStateValue() {
        switch (game.getModeAI()) {

            case 1:
                countNumberOfHoles();
                countNumberOfChangesInsideRows();
                countNbPuits();

                this.stateValue = this.numberOfClearedLines * CLEARED_LINES_WEIGHT;
                this.stateValue += this.newAddedPieceHeight * LOCKED_PIECE_HEIGHT;
                this.stateValue += this.numberOfWells * NUMBER_OF_WELL_CELL_WEIGHT;
                this.stateValue += this.numberOfHoles * NUMBER_OF_CLOSED_HOLES_WEIGHT;
                this.stateValue += this.numberOfChangesInsideRows * NUMBER_OF_ROW_CHANGES;
                return stateValue;


            case 2:
                countNumberOfHoles();
                countNumberOfChangesInsideRows();
                countNbPuits();

                this.stateValue = this.newAddedPieceHeight * LOCKED_PIECE_HEIGHT;
                this.stateValue += this.numberOfWells * NUMBER_OF_WELL_CELL_WEIGHT;
                this.stateValue += this.numberOfHoles * NUMBER_OF_CLOSED_HOLES_WEIGHT;
                this.stateValue += this.numberOfChangesInsideRows * NUMBER_OF_ROW_CHANGES;
                this.stateValue += Math.pow(CLEARED_LINES_WEIGHT, this.numberOfClearedLines);

                return stateValue;


            case 3:
                countNumberOfHoles();
                countNumberOfChangesInsideRows();
                countNbPuits();
                countTetrisWellHeight();

                this.stateValue = this.newAddedPieceHeight * LOCKED_PIECE_HEIGHT;
                this.stateValue += this.numberOfWells * NUMBER_OF_WELL_CELL_WEIGHT;
                this.stateValue += this.numberOfHoles * NUMBER_OF_CLOSED_HOLES_WEIGHT;
                this.stateValue += this.numberOfChangesInsideRows * NUMBER_OF_ROW_CHANGES;
                this.stateValue += this.tetrisWellHeight * TETRIS_WELL_HEIGHT;
                this.stateValue += Math.pow(CLEARED_LINES_WEIGHT, this.numberOfClearedLines);

                return stateValue;
        }
        return -1;
    }



    public void countNbPuits() {
        this.numberOfWells = 0;

        //enregistre le plus haut de chaque colonne
        int top[] = new int[COL_COUNT];
        for (int col = 0; col < COL_COUNT; col++) {
            top[col] = ROW_COUNT; // sans bloc, c'est le plus bas

            for (int row = 0; row < ROW_COUNT; row++) {
                if (aiTiles[row][col]) {
                    top[col] = row;
                    break;
                }
            }
        }


        for (int col = 0; col < COL_COUNT; col++) {
            int hauteurDepart;

            if (col == 0) {
                hauteurDepart = top[1];
            } else if (col + 1 == COL_COUNT) {
                hauteurDepart = top[COL_COUNT - 2];
            } else {
                hauteurDepart = Math.max(top[col - 1], top[col + 1]);
            }


            for (int row = hauteurDepart; row < top[col]; row++) {
                if ((col == 0 || aiTiles[row][col - 1]) && (col + 1 == COL_COUNT || aiTiles[row][col + 1])) {
                    this.numberOfWells++;
                }
            }

        } //for
    }

    /**
     *  D�tecte la heuteur d'un puits unique partant du bas du board. Ajoute des points selon une fonction quadratique.  
     *  Une hauteur de 8 est optimale.
     */
    public void countTetrisWellHeight() {
        int nbPuitsBas = 0;
        int colPuits = 0;
        int hauteur = 0;
        double points = 0;
        boolean lignePuit = true;

        for (int col = 0; col < COL_COUNT; col++) {
            if (!(aiTiles[ROW_COUNT - 1][col])) {
                nbPuitsBas++;
                colPuits = col;
            }
        }
        // System.out.println("nbPuitsBas " + nbPuitsBas);
        // System.out.println("colPuits " + colPuits);
        if (nbPuitsBas == 1) {
            //passer toutes les rangees....verifieer si seulement col est vide....
            for (int row = ROW_COUNT - 1; row >= 0 && lignePuit == true; row--) {
                for (int col = 0; col < COL_COUNT; col++) {
                    if (col == colPuits) {
                        if (aiTiles[row][col] == true) {
                            lignePuit = false;
                            //System.out.println("trou plein");
                        }
                    } else {
                        if (aiTiles[row][col] == false) {
                            lignePuit = false;
                            //System.out.println("ligne vide");
                        }
                    }
                }
                if (lignePuit == true) {
                    hauteur++;
                }
            }
            for (int row = ROW_COUNT - 1; row >= 0; row--) {
                if (aiTiles[row][colPuits] == true) {
                    hauteur = 0;
                }
            }

            this.tetrisWellHeight = hauteur;
        } else {
            this.tetrisWellHeight = 0;
            return;
        }

        points = (hauteur * 20);
//        if (hauteur > 3) {
//            points -= (20 * hauteur - 3);
//        }
        if (hauteur > 4) {
            points = 0;
        }
        //System.out.println("hauteur puits tetris = " + hauteur);
        //System.out.println("VALEUR AJOUTEE = " + points);
        this.tetrisWellHeight = (int) points;
    }


    /**
     * retourne le nombre de trou de l'ai board et aussi le nombre de changement a l'interieur d'une colonnes
     */
    public void countNumberOfHoles() {
        this.numberOfHoles = 0;

        for (int col = 0; col < COL_COUNT; col++) {

            boolean emptyTile = true; // Vrai lorsqu'on se promene dans le vide
            boolean firstTile = false; // Flag pour trouver le premier tile - le premier tile du haut compte pour rien

            for (int row = 0; row < ROW_COUNT; row++) {

                if (firstTile) { //Si nous sommes en dessous du bloc du haut...
                    if (emptyTile && aiTiles[row][col]) { // lorsque c'est vide et on frappe un bloc
                        emptyTile = false;
                    }

                    if (!emptyTile && !aiTiles[row][col]) { // lorsque c'est plein et on frappe du vide
                        this.numberOfHoles++;
                        emptyTile = true;
                    }

                } else if (aiTiles[row][col]) { //Sinon, si c'est un bloc = c'est le 1er bloc du haut
                    firstTile = true;
                    emptyTile = false;
                }

            }
        }

    }


    /**
     * le nombre de changement a l'interieur d'une rangee
     */
    private void countNumberOfChangesInsideRows() {
        this.numberOfChangesInsideRows = 0;

        for (int row = 0; row < ROW_COUNT; row++) {

            boolean emptyTile = false; // Si on traverse un tile vide - commence a false parce que le mur compte
            boolean blocACeNiveau = false; // Vrai si on trouve un tile a ce niveau
            int changements = 0; // Le nombre de changement vues a ce niveau, il sera dditionner au total seulement
            // s'il y a un minimum d'un bloc sur le niveau.

            for (int col = 0; col < COL_COUNT; col++) {

                if (!blocACeNiveau && aiTiles[row][col]) { // flag qu'il y a au minimum 1 bloc a ce niveau
                    blocACeNiveau = true;
                }

                if (emptyTile && aiTiles[row][col]) { // tile apres un vide = +1 changement
                    changements++;
                    emptyTile = false;
                }

                if (!emptyTile && !aiTiles[row][col]) { // vide apres un tile = +1 changement
                    changements++;
                    emptyTile = true;
                }

                if (emptyTile && col + 1 == COL_COUNT) { //un vide juste avant le mur = +1 changement
                    changements++;
                }
            } // for col


            //additionne les changements d'une range juste s'il y a eu au minimum un bloc sur le niveau
            if (blocACeNiveau) {
                this.numberOfChangesInsideRows += changements;
            }

        } //for row
    }



    /**
     * Retourne un ai board state avec la piece specifie a la colonne et rotation demande.
     */
    AIBoardState getNewBoardWithAddedPiece(AIAction action) {
        
        if (!action.isValid()) { // si la colonne est invalide - retourne null.
            return null;
        }

        AIBoardState nextBoardState = null;

        // Fais tomber la piece jusqu'a ce qu'elle frappe un block, l'idee est de trouver la hauteur ou elle est supposer aller
        // avec la rotation donne en parametre
        for (int i = 0; i < ROW_COUNT; i++) {

            if (!isValidSpot(action, i + 1)) { // verifie la ligne d'apres.

                nextBoardState = new AIBoardState(this, action);
                nextBoardState.addPieceToAIBoard(action, i);

                nextBoardState.newAddedPieceHeight = Math.abs(i - ROW_COUNT);

                break;
            }
        }

        return nextBoardState;
    }


    /**
     * retourne vrai si la ligne est valide & aucune conflits avec les tiles.
     */
    private boolean isValidSpot(AIAction action, int ligneDepart) {
        //s'assure que la ligneDepart est valide.
        if (ligneDepart < -action.piece.getTopInset(action.rotation) || ligneDepart + action.piece.getDimension() - action.piece.getBottomInset(action.rotation) >= ROW_COUNT) {
            return false;
        }

        /*
         * Passe au travers de toutes les 'tiles' de la piece pour voir s'il n'y a pas un conflit avec un tile existant.
         */
        int leftInset = action.piece.getLeftInset(action.rotation);
        for (int col = 0; col < action.piece.getDimension() - leftInset; col++) {
            for (int row = 0; row < action.piece.getDimension(); row++) {
                if (action.piece.isTile(col + leftInset, row, action.rotation) && aiTiles[ligneDepart + row][action.colonne + col]) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Ajoute une piece au ai board state. Toutes les tiles constituant la piece dependant de sa rotation
     * sont mis a true. Ensuite, effectue les changements au niveau des lignes pleines
     */
    private void addPieceToAIBoard(AIAction action, int ligneDepart) {
        /*
         * Loop through every tile within the piece and add it
         * to the board only if the boolean that represents that
         * tile is set to true.
         */
        int leftInset = action.piece.getLeftInset(action.rotation);
        for (int col = 0; col < action.piece.getDimension() - leftInset; col++) {
            for (int row = 0; row < action.piece.getDimension(); row++) {
                if (action.piece.isTile(col + leftInset, row, action.rotation)) {
                    aiTiles[ligneDepart + row][action.colonne + col] = true;
                }
            }
        }


        /*
         * Loop au travers toutes les lignes. Si une est pleine, on increment le nombre de ligne pleine fait
         * dans cette etat et ensuite on shift tous les blocs
         */
        for (int row = 0; row < ROW_COUNT; row++) {
            if (isLineCompleted(row)) {
                this.numberOfClearedLines++;
                shiftAllRowsDownByOne(row);
            }
        }
    }



    /**
     * Retourne vrai si la ligne en cours est pleine.
     */
    private boolean isLineCompleted(int line) {

        //Loop toutes les colonnes, si une n'est pas pleine, alors on retourne false
        for (int col = 0; col < COL_COUNT; col++) {
            if (!aiTiles[line][col]) {
                return false;
            }
        }

        return true;
    }


    /**
     * Shift toutes les lignes a partir de line une coche plus bas dans le board
     */
    private void shiftAllRowsDownByOne(int line) {
        for (int ligne = line - 1; ligne >= 0; ligne--) {
            System.arraycopy(aiTiles[ligne], 0, aiTiles[ligne + 1], 0, COL_COUNT);
        }
    }


    /**
     * Copy l'etat du board du jeu sur celui ai.
     */
    public void copyFromRealBoard() {

        /*
         * Copy l'etat de chaque cellule a savoir s'il sont occupe ou non.
         */
        for (int y = 0; y < COL_COUNT; y++) {
            for (int x = 0; x < ROW_COUNT; x++) {
                aiTiles[x][y] = game.isOccupiedOnRealBoard(x, y);
            }
        }
    }


    /**
     * Resets le ai board.
     */
    public void clear() {
        /*
         * Met tout le tableau ai a false incluant les topRows
         */
        for (int y = 0; y < COL_COUNT; y++) {
            for (int x = 0; x < ROW_COUNT; x++) {
                aiTiles[x][y] = false;
            }
        }
    }


    @Override
    public String toString() {

        String result = "";

        for (int x = 0; x < ROW_COUNT; x++) {
            for (int y = 0; y < COL_COUNT; y++) {
                result += (aiTiles[x][y]) ? "*" : "0";
            }
            result += "\n";
        }

        return result;
    }

    void printDebugInfo() {
        System.out.println("AIBoardState - ClearedLines:" + Integer.toString(this.numberOfClearedLines) +
            " PieceHeight :" + Integer.toString(this.newAddedPieceHeight) +
            " Nombre de trous :" + Integer.toString(this.numberOfHoles) +
            " Nombre puits:" + Integer.toString(this.numberOfWells) +
            " Changements rangees :" + Integer.toString(this.numberOfChangesInsideRows) +
            " stateValue :" + Integer.toString(this.stateValue) + "\n");
    }


    /***************************************************
     * PLUS BAS = Functions 'package' utile pour les test unitaire - on pourra peut etre deleter le tout
     * avant la remise du tp3
     */

    //Contructor
    AIBoardState() {
        aiTiles = new boolean[ROW_COUNT][COL_COUNT];
        actionFromParent = null;
        etatPrecedent = null;

        numberOfClearedLines = 0;
        newAddedPieceHeight = 0;
        numberOfHoles = 0;
        numberOfChangesInsideRows = 0;
        numberOfWells = 0;

        stateValue = 0;
    }

    boolean[][] getAiTiles() {
        return aiTiles;
    }

    int getNumberOfClearedLines() {
        return numberOfClearedLines;
    }

    int getNewAddedPieceHeight() {
        return newAddedPieceHeight;
    }

    int getNumberOfHoles() {
        return numberOfHoles;
    }

    int getNumberOfWells() {
        return numberOfWells;
    }

    int getNumberOfChangesInsideRows() {
        return numberOfChangesInsideRows;
    }

}
