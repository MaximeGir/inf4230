package tetris.ai;

/*
 * INF4230 - Intelligence artificielle
 * 
 * L'equipe Crocodile - Steve Boucher & friends
 * 
 * AIAction - Represente un action a faire sur un AIBoardState. 
 *            Donc une piece, une colonne et une rotation
 */


import tetris.TileType;
import static tetris.ai.AIBoardState.COL_COUNT;

/**
 *
 * @author mathieu
 */
public class AIAction {
    final TileType piece;
    public int colonne;
    final public int rotation;

    public AIAction(TileType piece, int colonne, int rotation) {
        this.piece = piece;
        this.colonne = colonne;
        this.rotation = rotation;
    }
    
    public boolean isValid(){
         //s'assure que la colonneDepart n'est trop a gauche.
        if (colonne < -piece.getLeftInset(rotation)) { // pas sur que ce bout soit utile dans notre ca, ca vient du code original de tetris
            return false;
        }

        //s'assure que la colonneDepart va pas buster sur la droite
        if (colonne + piece.getDimension() - piece.getLeftInset(rotation) - piece.getRightInset(rotation) >= COL_COUNT) {
            return false;
        }
        return true;
    }
    
    @Override
    public AIAction clone(){
        return new AIAction(piece, colonne, rotation);
    }
    
    public String toString(){
        return piece.getBaseColor() + " " + colonne + " " + rotation;
    }
}
