package tetris.ai;

import tetris.TileType;

/*
 * INF4230 - Intelligence artificielle
 * 
 * L'equipe Crocodile - Steve Boucher & friends
 * 
 * AIPlayerThread - Represente l'AI du joueur. On a pas le choix de le partir
 * dans un thread separe si on veut vraiment simuler un joueur pense pendant
 * que le jeu de deroule et que l'on ne veut pas *jammer* les blocs pendant
 * qu'ils tombent. 
 */

class AIPlayerThread extends Thread {

    private AIController game;          // le pont vers le jeu tetris,board,etc

    private AIBoardState aiBoardCurrentState;        // le board - pour le traitement ai
    
    private boolean blocEnMouvement;    // Il y a un bloc qui est maintenant en train de tomber

            
    /**
     * Constructeur
     * @param game        le game - thread parent et aussi le pont avec le jeu tetris
     */
    public AIPlayerThread(AIController game) {
        
        this.game = game;
        
        this.blocEnMouvement = false;       //au debut, il n'y a aucune piece qui tombe
        
        aiBoardCurrentState = new AIBoardState(game);
        aiBoardCurrentState.clear();                    //au debut, le board est vide
    }


    /**
     * Cette fonction est lance par le start du thread. 
     */
    @Override
    public void run() {
        
        /**
         * AI Loop - tient le thread actif tant que la game n'est pas fini
         */
        while (!game.isGameOver()) {
            
            // Met l'ai sur pause pendant que la vrai game est sur pause.
            pauseAIDuringGamePause();

            /**
             * Les operation quand une piece tombe
             *   L'algo de hill-climbing est la dedans
             */
            executeFallingPieceOperations();           // Les operation quand une piece tombe
            
            /**
             * Les operations a faire entre deux pieces
             *   Rafraichir le du ai board est la dedans
             */
            executeDropCooldownModeOperations();


            //Pour ne pas etre si intense au niveau du CPU.
            pauseThread(1);
        }

    }

    
    
    
   /**
    * S'occupe des operations lorsqu'une piece est en train de tomber.
    */
    private void executeFallingPieceOperations() {
        if (!game.isInDropCooldown()  &&  !blocEnMouvement) {
            
            /**
             * flag que la piece est en train de tomber - s'assure aussi que l'on va passer une seule fois
             * dans cette fonction par piece.
             */

            blocEnMouvement = true;
            
            TileType [] pieces = new TileType[2];
            pieces[0] = game.getPieceType();
            pieces[1] = game.getNextPieceType();
            
            HillClimbing algo = new HillClimbing(pieces,2);
            algo.execute(aiBoardCurrentState, 1);

            playMoves(algo.getAction());
        }
    }

    
    
    /**
     * S'occupe des operations lorsque le jeu est en dropCooldown.
     */
    private void executeDropCooldownModeOperations() {

        if (game.isInDropCooldown()  &&  blocEnMouvement) {
            /**
             * flag que la piece a fini de tomber - s'assure aussi que l'on va passer dans cette function
             * une seule fois par cooldown.
             */
            blocEnMouvement = false;
            
            
            aiBoardCurrentState.copyFromRealBoard();    //Rafraichi le aiBoardCurrentState avec l'etat du vrai board
        }
    }

    
    
    /**
     *  Envoi les commandes au jeu de tetris pour bouger la piece a la bonne colonne
     */
    public void playMoves(AIAction action) {
        pauseThread(25);
        
        while (game.getPieceRotation() < action.rotation) {
            while (game.getPieceRotation() != action.rotation) {
                game.playAIMove(AIPossibleMove.ANTICLOCKWISE);
                pauseThread(5);
            }
        }
        
        while (game.getPieceRotation() > action.rotation) {
            while (game.getPieceRotation() != action.rotation) {
                game.playAIMove(AIPossibleMove.CLOCKWISE);
                pauseThread(5);
            }
        }

        while (game.getPieceColumn() < action.colonne) {
            game.playAIMove(AIPossibleMove.RIGHT);
            pauseThread(5);
        }

        while (game.getPieceColumn() > action.colonne) {
            game.playAIMove(AIPossibleMove.LEFT);
            pauseThread(5);
        }

        
        game.playAIMove(AIPossibleMove.DROP);
        
    }
    
    
    
    
    /**
     * Met le AI sur pause quand la game est sur pause
     */
    private void pauseAIDuringGamePause() {
        
        while(game.isPaused()  &&  !game.isGameOver()) {
            // empty
        }
    }

    /**
     * Fait une pause du player thread - utiliser dans playMoves surtout pour permettre
     * au thread du jeu tetris de faire les moves demander (comme les rotations)
     */
    private void pauseThread(int millisecond) {
        try {
            Thread.sleep(millisecond);
        } catch (InterruptedException ex) {
        }
    }
    
}
