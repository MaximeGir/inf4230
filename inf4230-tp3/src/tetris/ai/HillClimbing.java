package tetris.ai;

import java.util.ArrayList;

import tetris.TileType;

/**
 * INF4230 - Intelligence artificielle
 * 
 * L'equipe Crocodile - Steve Boucher & friends
 * 
 * HillClimbing - S'occupe de la poutine de l'algorithme de Hill Climbing
 * 
 */
public class HillClimbing {

    int onGoingHillClimbings = 0;
    
    AIBoardState bestboard;
    
    /*int colonneVoulue;
    int rotationVoulue;
    int stateValue;*/
    int maxDepth;
    TileType pieces[];
    
    ArrayList<Thread> children = new ArrayList();

    HillClimbing(TileType[] pieces, int maxDepth) {
        this.pieces = pieces;
        this.maxDepth = maxDepth;
    }
    
    public AIAction getAction(){
        AIAction action = bestboard.etatPrecedent.actionFromParent.clone();
        action.colonne = action.colonne - action.piece.getLeftInset(action.rotation);
        return action;
    }
    
    
    /**
     * Execute l'algorithme du HillClimbing - trouve le meilleur placement de la piece qui tombe
     * en tenant compte de la prochaine piece.
     * @param board - l'etat courant du AI Board
     * @param depth - La profondeur que l'on commence
     */
    public void execute(AIBoardState board, int depth) {
        // Si ces conditions ne sont pas atteintes, on ne continue pas. On tue le thread

        if(board == null || depth > maxDepth) return;
        
        AIBoardState nextboard;

        for(int col=0; col < AIBoardState.COL_COUNT; col++) {            
            for(int rotation=0; rotation < 4; rotation++){
                
                nextboard = board.getNewBoardWithAddedPiece(new AIAction(pieces[depth-1], col, rotation));
                if(nextboard == null) continue;

                if(depth == maxDepth && bestboard == null){
                    bestboard = nextboard;
                } else if(depth == maxDepth && (nextboard.getSecondBlocStateValue() + nextboard.etatPrecedent.getFirstBlockStateValue()) > (bestboard.getSecondBlocStateValue() + bestboard.etatPrecedent.getFirstBlockStateValue())){
                    bestboard = nextboard;
                }
                
                execute(nextboard, depth+1);

            }//for rotation
        }//for col
    }
}
