package tetris.ai;

import tetris.*;

/**
 * INF4230 - Intelligence artificielle
 * 
 * L'equipe Crocodile - Steve Boucher & friends
 * 
 * AIController - La classe qui represente le joueur artificiel
 * Pour l'instant, il ne fait que le pont avec le jeu Tetris et le thread du AI.
 * 
 */
public class AIController {

    private AIPlayerThread thread;
    private Tetris tetris;
    private BoardPanel board;
    
    /**
     * Constructeur
     * @param tetris   - le jeu tetris
     * @param board    - le vrai board
     */
    public AIController(Tetris tetris, BoardPanel board) {

        this.tetris = tetris;
        this.board = board;
    }
    
    
    /**
     * reset l'AI \ chaque de but de partie. Monte et start le thread qui constitue l'AI
     */
    public void resetAIPlayerForNewGame() {
        this.thread = new AIPlayerThread(this);
        this.thread.start();
    }

    
    /**
     * Joue un move sur le vrai board
     * @param aiMove 
     */
    void playAIMove(AIPossibleMove aiMove) {
        tetris.playAIMove(aiMove);
    }

    
    /**
     * Verifie sur le vrai board si le tile est occupe
     * @param aiMove 
     */
    boolean isOccupiedOnRealBoard(int x, int y) {
        /**
         * ici les x,y sont inverse intentionnellement. Une chose wierd que j'ai vu du programmeur
         * qui a fait le jeu de tetris.
         */
        return board.isOccupied(y, x);
    }

    /**
     * retourne si la game est sur pause
     */
    boolean isPaused() {
        return tetris.isPaused();
    }

    /**
     * retourne si la game est est en cooldown - entre deux pieces
     */
    boolean isInDropCooldown() {
        return tetris.isInDropCooldown();
    }

    /**
     * retourne si la game est over
     */
    boolean isGameOver() {
        return tetris.isGameOver();
    }
    
    /**
     * retourne la piece en cours qui tombe
     */
    TileType getPieceType() {
        return tetris.getPieceType();
    }    

    /**
     * retourne la prochaine piece qui tombe
     */
    TileType getNextPieceType(){
    	return tetris.getNextPieceType();
    }
    
    /**
     * retourne le colonne de la piece qui tombe.
     */
    public int getPieceColumn() {
        return tetris.getPieceCol();
    }
    
    public int getPieceRotation(){
        return tetris.getPieceRotation();
    }
    /**
     * retourne le mode AI.
     */
    public int getModeAI(){
    	return tetris.getAI_Mode();
    }
    /**
     * retourne le mode de drop.
     */
    public int getDropMode(){
    	return tetris.getDrop_Mode();
    }
    
}
