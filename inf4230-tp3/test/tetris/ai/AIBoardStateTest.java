package tetris.ai;

import org.junit.*;
import static org.junit.Assert.*;
import tetris.TileType;


public class AIBoardStateTest {
    
    AIBoardState state;
    boolean[][] aiTiles;

    @Before
    public void setUp() {
        state = new AIBoardState();
        aiTiles = state.getAiTiles();
    }
    
    @After
    public void tearDown() {
        state = null;
        aiTiles = null;
    }

    
    @Test
    public void testLockedPieceHeight1() {
        
        AIBoardState newState1 = state.getNewBoardWithAddedPiece(TileType.TypeO, 0, 0);        // carre a gauche
        int height = newState1.getNewAddedPieceHeight();
        assertEquals(2, height);
        
        AIBoardState newState2 = newState1.getNewBoardWithAddedPiece(TileType.TypeO, 0, 0);   //carre par dessus l'autre
        height = newState2.getNewAddedPieceHeight();
        assertEquals(4, height);
        
        AIBoardState newState3 = newState2.getNewBoardWithAddedPiece(TileType.TypeO, 3, 0);  // carre a cote
        height = newState3.getNewAddedPieceHeight();
        assertEquals(2, height);
        
        AIBoardState newState4 = newState3.getNewBoardWithAddedPiece(TileType.TypeO, 3, 0);  // carre par dessus celui d'a cote
        height = newState4.getNewAddedPieceHeight();
        assertEquals(4, height);
    }
    
    @Test
    public void testClearedLines() {
        int bottom = AIBoardState.ROW_COUNT-1;
        
        //rempli les 2 dernier ligne du bas
        for(int row=0; row<2; row++)
            for(int col=0; col<AIBoardState.COL_COUNT; col++)
                aiTiles[bottom-row][col] = true;

        aiTiles[bottom][0] = false;
        aiTiles[bottom][1] = false;
        aiTiles[bottom-1][0] = false;
        aiTiles[bottom-1][1] = false;
        
        AIBoardState newState1 = state.getNewBoardWithAddedPiece(TileType.TypeO, 4, 0);
        assertEquals(0, newState1.getNumberOfClearedLines());

        AIBoardState newState2 = newState1.getNewBoardWithAddedPiece(TileType.TypeO, 0, 0);

        assertEquals(2, newState2.getNumberOfClearedLines());
        
    }
    
    
    @Test
    public void testClosedHoles1() {
        int bottom = AIBoardState.ROW_COUNT-1;
        
        //rempli les 4 dernier ligne du bas
        for(int row=0; row<4; row++)
            for(int col=0; col<AIBoardState.COL_COUNT; col++)
                aiTiles[bottom-row][col] = true;
       
        //1 trou a gauche en bas
        aiTiles[bottom][0] = false;
        state.getSecondBlocStateValue();
        assertEquals(1, state.getNumberOfHoles());
        
        //1 trou a droite en bas
        aiTiles[bottom][AIBoardState.COL_COUNT-1] = false;
        state.getSecondBlocStateValue();
        assertEquals(2, state.getNumberOfHoles());

        //1 trou dans le mileu en bas
        aiTiles[bottom][4] = false;
        state.getSecondBlocStateValue();
        assertEquals(3, state.getNumberOfHoles());
        
        //3 gros trou en bas
        aiTiles[bottom-1][0] = false;
        aiTiles[bottom-1][4] = false;
        aiTiles[bottom-1][AIBoardState.COL_COUNT-1] = false;
        state.getSecondBlocStateValue();
        assertEquals(3, state.getNumberOfHoles());

        //3 trou, mais pas completement en bas
        aiTiles[bottom][0] = true;
        aiTiles[bottom][4] = true;
        aiTiles[bottom][AIBoardState.COL_COUNT-1] = true;
        state.getSecondBlocStateValue();
        assertEquals(3, state.getNumberOfHoles());

        //3 gros trou, mais pas completement en bas
        aiTiles[bottom-2][0] = false;
        aiTiles[bottom-2][4] = false;
        aiTiles[bottom-2][AIBoardState.COL_COUNT-1] = false;
        state.getSecondBlocStateValue();
        assertEquals(3, state.getNumberOfHoles());
    
        //defait les trois trous en les mettants en puits
        aiTiles[bottom-3][0] = false;
        aiTiles[bottom-3][4] = false;
        aiTiles[bottom-3][AIBoardState.COL_COUNT-1] = false;
        state.getSecondBlocStateValue();
        assertEquals(0, state.getNumberOfHoles());
    }
    

    @Test
    public void testChangesInsidesRows1() {
        int bottom = AIBoardState.ROW_COUNT-1;
        
        //rempli les 7 dernier ligne du bas
        for(int row=0; row<7; row++)
            for(int col=0; col<AIBoardState.COL_COUNT; col++)
                aiTiles[bottom-row][col] = true;
       
        //0 changement
        state.getSecondBlocStateValue();
        assertEquals(0, state.getNumberOfChangesInsideRows());

        //1 changement a gauche en bas
        aiTiles[bottom][0] = false;
        state.getSecondBlocStateValue();
        assertEquals(2, state.getNumberOfChangesInsideRows());
        
        //1 changement a gauche en bas
        aiTiles[bottom][AIBoardState.COL_COUNT-1] = false;
        state.getSecondBlocStateValue();
        assertEquals(4, state.getNumberOfChangesInsideRows());
        
        //1 changement dans milieu en bas
        aiTiles[bottom][4] = false;
        state.getSecondBlocStateValue();
        assertEquals(6, state.getNumberOfChangesInsideRows());
        
        //3 changement plus gros
        aiTiles[bottom][1] = false;
        aiTiles[bottom][5] = false;
        aiTiles[bottom][AIBoardState.COL_COUNT-2] = false;
        state.getSecondBlocStateValue();
        assertEquals(6, state.getNumberOfChangesInsideRows());

        //3 autre changement
        aiTiles[bottom-1][1] = false;
        aiTiles[bottom-1][5] = false;
        aiTiles[bottom-1][AIBoardState.COL_COUNT-2] = false;
        state.getSecondBlocStateValue();
        assertEquals(12, state.getNumberOfChangesInsideRows());
    }
    
    
    @Test
    public void testWellCellsCount() {
        int bottom = AIBoardState.ROW_COUNT-1;
        
        //rempli les 3 dernier ligne du bas
        for(int row=0; row<3; row++)
            for(int col=0; col<AIBoardState.COL_COUNT; col++)
                aiTiles[bottom-row][col] = true;
       
        //0 changement
        state.getSecondBlocStateValue();
        assertEquals(0, state.getNumberOfWells());

        //3 changement en dessous
        aiTiles[bottom-1][0] = false;
        aiTiles[bottom-1][AIBoardState.COL_COUNT-1] = false;
        aiTiles[bottom-1][4] = false;
        state.getSecondBlocStateValue();
        assertEquals(0, state.getNumberOfWells());
        
        //3 changement en en puit de 2
        aiTiles[bottom-2][0] = false;
        aiTiles[bottom-2][AIBoardState.COL_COUNT-1] = false;
        aiTiles[bottom-2][4] = false;
        state.getSecondBlocStateValue();
        assertEquals(6, state.getNumberOfWells());
    
        //3 changement en en puit de 1
        aiTiles[bottom-1][0] = true;
        aiTiles[bottom-1][AIBoardState.COL_COUNT-1] = true;
        aiTiles[bottom-1][4] = true;
        state.getSecondBlocStateValue();
        assertEquals(3, state.getNumberOfWells());
    
        //3 changement en en puit de 1
        aiTiles[bottom-1][0] = false;
        aiTiles[bottom-1][AIBoardState.COL_COUNT-1] = false;
        aiTiles[bottom-1][4] = false;
        aiTiles[bottom][0] = false;
        aiTiles[bottom][AIBoardState.COL_COUNT-1] = false;
        aiTiles[bottom][4] = false;
        state.getSecondBlocStateValue();
        assertEquals(9, state.getNumberOfWells());
    }
    
}




/*
 * POSSIBLE IMPROVEMENT - faire en sorte que le bas soit une transition
 * 
        int CLEARED_LINES_WEIGHT = -1;              // Le nombre de lignes complete dans l'etat courant

* 
 * public void testGetStateValue() {
    public void testGetNewBoardWithAddedPiece() {
    public void testCopyFromRealBoard() {
    public void testClear() {
    * 
    */








