package tetris.ai;

import org.junit.*;
import static org.junit.Assert.*;
import tetris.TileType;


public class HillClimbingTest {
    
    HillClimbing algo;
    AIBoardState board;
    boolean[][] aiTiles;
    
    
    @Before
    public void setUp() {
        algo = new HillClimbing();
        board = new AIBoardState();
        aiTiles = board.getAiTiles();
    }
    
    
    @Test
    public void testExecuteBlocISurLaGauche1() {
        int bottom = AIBoardState.ROW_COUNT-1;
        int right = AIBoardState.COL_COUNT-1;
        
        //rempli les 4 dernier ligne du bas, sauf la premier colonne
        for(int row=0; row<4; row++)
            for(int col=1; col<AIBoardState.COL_COUNT; col++)
                aiTiles[bottom-row][col] = true;
        
     
        algo.execute(TileType.TypeI, TileType.TypeI, board);
        
        assertEquals(-2, algo.colonneVoulue);
        assertEquals(1, algo.rotationVoulue);
        
    }

    
    @Test
    public void testExecuteBlocISurLaDroite1() {
        int bottom = AIBoardState.ROW_COUNT-1;
        int right = AIBoardState.COL_COUNT-1;
        
        //rempli les 4 dernier ligne du bas, sauf la premier colonne
        for(int row=0; row<4; row++)
            for(int col=0; col<AIBoardState.COL_COUNT-1; col++)
                aiTiles[bottom-row][col] = true;
        
        algo.execute(TileType.TypeI, TileType.TypeI, board);
        
        assertEquals(7, algo.colonneVoulue);
        assertEquals(1, algo.rotationVoulue);
        
    }
    
    
    @Test
    public void testExecuteBlocJSurLaGauche1() {
        int bottom = AIBoardState.ROW_COUNT-1;
        int right = AIBoardState.COL_COUNT-1;
        
        //rempli les 2 dernier ligne du bas, sauf la premier colonne
        for(int row=0; row<2; row++)
            for(int col=1; col<AIBoardState.COL_COUNT; col++)
                aiTiles[bottom-row][col] = true;
        
    
        algo.execute(TileType.TypeJ, TileType.TypeJ, board);
        
        assertEquals(-1, algo.colonneVoulue);
        assertEquals(1, algo.rotationVoulue);
        
    }

    @Test
    public void testExecuteBlocJSurLaDroite1() {
        int bottom = AIBoardState.ROW_COUNT-1;
        int right = AIBoardState.COL_COUNT-1;
        
        //rempli les 2 dernier ligne du bas, sauf la premier colonne
        for(int row=0; row<2; row++)
            for(int col=0; col<AIBoardState.COL_COUNT-1; col++)
                aiTiles[bottom-row][col] = true;
        
     
        algo.execute(TileType.TypeJ, TileType.TypeJ, board);
        
        assertEquals(7, algo.colonneVoulue);
        assertEquals(2, algo.rotationVoulue);
        
    }
    
}








