package connect5.ia;

import connect5.Grille;
import connect5.Position;

public class Successeur {

	private Grille grille;
	private Position position;
	
	public double v;
	
	public Successeur(Grille grille, Position position){
		this.grille = grille;
		this.position = position;
	}
	
	public Position getPosition(){
		return this.position;
	}
	
	public Grille getGrille(){
		return this.grille;
	}
}


