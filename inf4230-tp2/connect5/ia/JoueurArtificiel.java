package connect5.ia;

/*
 * Si vous utilisez Java, vous devez modifier ce fichier-ci.
 *
 * Vous pouvez ajouter d'autres classes sous le package connect5.ia.
 *
 * PrÃ©nom Nom    (CODE00000001)
 * PrÃ©nom Nom    (CODE00000002)
 */

import connect5.Grille;
import connect5.GrilleVerificateur;
import connect5.Joueur;
import connect5.Position;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class JoueurArtificiel implements Joueur {

	public static ArrayList<Integer> getCasesVides(Grille grille) {
		ArrayList<Integer> casesvides = new ArrayList<Integer>();
		int nbcol = grille.getData()[0].length;
		for (int l = 0; l < grille.getData().length; l++)
			for (int c = 0; c < nbcol; c++)
				if (grille.getData()[l][c] == 0)
					casesvides.add(l * nbcol + c);
		return casesvides;
	}

	public ArrayList<Successeur> getSuccesseurs(Grille etat) {
		int joueur;
		int j = (etat.getSize() - etat.nbLibre()) % 2;
		//int j = getCasesVides(etat).size() % 2;
		
		if(j == 0){
			joueur = 1;
		}else{
			joueur = 2;
		}
		ArrayList<Successeur> successeurs = new ArrayList<Successeur>();
        
		if(etat.nbLibre() == 0){ return successeurs; }
        
		int cols = etat.getData().length;
		ArrayList<Integer> liste = getCasesVides(etat);
		
		for(int position = 0; position < liste.size(); position++) {
			//if (position >= liste.size()) break;
			int positionx = liste.get(position);
			Position positionInterim = new Position(positionx / cols, positionx%cols);
			Grille etatSuccesseur = etat.clone();
			etatSuccesseur.set(positionInterim, joueur);
			Successeur etatS = new Successeur(etatSuccesseur, positionInterim);
			successeurs.add(etatS);
		}
		Collections.sort(successeurs, new GrilleComparator());
		
		return successeurs;
	}

	public static int utility(Grille etat) {
		
		int value = 0;
		int lastValue = 0;
		int longeurLigne = 0;
		//ArrayList<Integer> casesVides = getCasesVides(etat);
		int joueur = (etat.getSize() - etat.nbLibre()) % 2;
		int lignesB[] = { 0, 0, 0, 0, 0 };
		int lignesN[] = { 0, 0, 0, 0, 0 };

		// horizontal
		for (int l = 0; l < etat.getData().length; l++){
			if (lastValue == 2 && longeurLigne < 6) {
				lignesB[longeurLigne-1]++;
			} else if (lastValue == 1 && longeurLigne < 6) {
				lignesN[longeurLigne-1]++; // crash possible ici
			}
			lastValue = 0;
			longeurLigne = 0;
			for (int c = 0; c < etat.getData()[0].length; c++) {
				value = etat.getData()[l][c];
				longeurLigne = compterLignes(value, lastValue, longeurLigne, lignesB, lignesN);
				lastValue = value;
			}
		}
		// vertical
		for (int c = 0; c < etat.getData()[0].length; c++) {
			if (lastValue == 2 && longeurLigne < 6) {
				lignesB[longeurLigne - 1]++;
			} else if (lastValue == 1 && longeurLigne < 6) {
				lignesN[longeurLigne - 1]++;  // crash possible ici
			}
			lastValue = 0;
			longeurLigne = 0;
			for (int l = 0; l < etat.getData().length; l++) {
				value = etat.getData()[l][c];
				longeurLigne = compterLignes(value, lastValue, longeurLigne, lignesB, lignesN);
				lastValue = value;
			}
		}
		// Diagonal \\\\\\\
        for (int c = -etat.getData().length; c < etat.getData()[0].length; c++) {
            int c2 = c;
            int l = 0;
            if (c2 < 0) {
                l = -c2;
                c2 = 0;
            }
            if (lastValue == 2 && longeurLigne < 6) {
				lignesB[longeurLigne - 1]++;
			} else if (lastValue == 1 && longeurLigne < 6) {
				lignesN[longeurLigne - 1]++;  // crash possible ici
			}
			lastValue = 0;
			longeurLigne = 0;
            for (; c2 < etat.getData()[0].length && l < etat.getData().length; c2++, l++) {
            	value = etat.getData()[l][c2];
				longeurLigne = compterLignes(value, lastValue, longeurLigne,
				lignesB, lignesN);
				lastValue = value; 	
            }
        }
        // Diagonal //////
        for (int c = -etat.getData().length; c < etat.getData()[0].length; c++) {
            int c2 = c;
            int l = etat.getData().length - 1;
            if (c2 < 0) {
                l += c2;
                c2 = 0;
            }
            if (lastValue == 2 && longeurLigne < 6) {
				lignesB[longeurLigne - 1]++;
			} else if (lastValue == 1 && longeurLigne < 6) {
				lignesN[longeurLigne - 1]++; // crash possible ici
			}
			lastValue = 0;
			longeurLigne = 0;
            for (; c2 < etat.getData()[0].length && l >= 0; c2++, l--) {
            	value = etat.getData()[l][c2];
				longeurLigne = compterLignes(value, lastValue, longeurLigne,
						lignesB, lignesN);
				lastValue = value;
            }
        }
        
		// Calcule l'utilitepour le joueur actuel.
        if(joueur == 1){
            //System.out.println("CAS1");

            return ((lignesB[0] * 1) + (lignesB[1] * 10) + (lignesB[2] * 100) + (lignesB[3] * 1000) + (lignesB[4] * 1000000)) -
                   ((lignesN[0] * 1) + (lignesN[1] * 10) + (lignesN[2] * 100) + (lignesN[3] * 1000) + (lignesN[4] * 1000000));
        }else{
            //System.out.println("CAS2");

            return ((lignesN[0] * 1) + (lignesN[1] * 10) + (lignesN[2] * 100) + (lignesN[3] * 1000) + (lignesN[4] * 1000000)) - 
                   ((lignesB[0] * 1) + (lignesB[1] * 10) + (lignesB[2] * 100) + (lignesB[3] * 1000) + (lignesB[4] * 1000000));      
        }

       
	}

	private static int compterLignes(int value, int lastValue,
			int longeurLigne, int[] lignesB, int[] lignesN) {
		//0 apres 0
		if ((value == 0) && (lastValue == 0)) {
			// System.out.println("un 0 apres un 0");
			return longeurLigne;
		//0 apres autre
		} else if ((value == 0) && (lastValue != 0)) {
			// System.out.println("un 0 apres autre");
			if ((longeurLigne > 0) && (longeurLigne < 6)) {
				if (lastValue == 2) {
					lignesB[longeurLigne - 1]++;
				}else if (lastValue == 1) {
					lignesN[longeurLigne - 1]++;
				}
				longeurLigne = 0;
			}
		//Joueur blanc apres 0
		} else if ((value == 2) && (lastValue == 0) && (longeurLigne < 6)) {
			longeurLigne = 1;
		//Joueur Blanc apres Noir
		}else if ((value == 2) && (lastValue == 1) && (longeurLigne < 6)) {
				lignesN[longeurLigne -1]++;
				longeurLigne = 1;
		//Joueur Noir apres 0
		}else if ((value == 1) && (lastValue == 0) && (longeurLigne < 6)) {
			longeurLigne = 1;
			//borneN = false;
		//Joueur Noir apres Blanc
		}else if ((value == 1) && (lastValue == 2) && (longeurLigne < 6)) {
				lignesB[longeurLigne -1]++;
				longeurLigne = 1;
		} else if ((value == 2) && (lastValue == 2)) {
			longeurLigne++;
		} else if ((value == 1) && (lastValue == 1)) {
			longeurLigne++;
		}
		return longeurLigne;
	}

	
	public double minimax_alpha_beta2(Grille grille, int profondeur, int maxProfondeur, double alpha, double beta, Position position, long tempsLimite) {
        GrilleVerificateur verificateur = new GrilleVerificateur();
        //System.out.println("v test: " + utility(grille));
        if((System.currentTimeMillis() >= tempsLimite) || profondeur <= 0 || verificateur.determineGagnant(grille) != 0){
            return utility(grille);
        }
        
        double best = Double.NEGATIVE_INFINITY;
        
        ArrayList<Successeur> successeurs = getSuccesseurs(grille);
        double[] score = new double[successeurs.size()];
        
        for(int i = 0; i < successeurs.size(); ++i){
            double v = -(minimax_alpha_beta2(successeurs.get(i).getGrille(), profondeur-1, maxProfondeur, -beta, -alpha, position, tempsLimite));

            if (v > best) {
                best = v;
                
                if (best > alpha) {
                    alpha = best;
                    
                    if (alpha >= beta) {
                        return best;
                    }
                }
            }
            
            if (profondeur == maxProfondeur) {
                score[i] = best;
            }
        }
        
        if (profondeur == maxProfondeur) {
            double meilleur = Double.POSITIVE_INFINITY;
            int index = 0;
            
            for (int i = 0; i < score.length; ++i) {                
                if (score[i] < meilleur) {
                    meilleur = score[i];
                    index = i;
                }             
            }
            
            position.colonne = successeurs.get(index).getPosition().colonne;
            position.ligne = successeurs.get(index).getPosition().ligne;
            //System.out.println("l = " + position.ligne + " c = " + position.colonne);
        }
        
        return best;
    }

	@Override
	public Position getProchainCoup(Grille etat, int delais) {

	if(etat.get(etat.getData().length/2, etat.getData()[0].length/2) == 0){
	Position position = new Position(etat.getData().length/2, etat.getData()[0].length/2);
	return position;
	}
	if(etat.get(etat.getData().length/2, (etat.getData()[0].length/2)-1) == 0){
	Position position = new Position(etat.getData().length/2, (etat.getData()[0].length/2)-1);
	return position;
	}

	//System.out.println(etat.toString());

	long tempsLimite = System.currentTimeMillis() + delais;

	    Position position = new Position();
	    
	    ArrayList<Position> positionsProfondeurs = new ArrayList<Position>();
	    double[] vProfondeurs = new double[100];
	    
	    for(int i = 0; System.currentTimeMillis() < tempsLimite; i++){
	        Position tempPosition = new Position();
	        //double v = minimax_alpha_beta(etat, i, i, true, position, tempsLimite);
	        double v = minimax_alpha_beta2(etat, i, i, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, position, tempsLimite);
	        tempPosition.colonne = position.colonne;
	        tempPosition.ligne = position.ligne;
	        positionsProfondeurs.add(tempPosition);
	        vProfondeurs[i] = v;
	    }
	    
	    double v = Double.NEGATIVE_INFINITY;
	    
	    for(int i = 0; i < positionsProfondeurs.size(); i++){    
	            if(vProfondeurs[i] > v){
	                v = vProfondeurs[i];
	                position.colonne = positionsProfondeurs.get(i).colonne; 
	                position.ligne = positionsProfondeurs.get(i).ligne; 
	            }
	        }   

	    return position;
	}
	
	@Override
	public String getAuteurs() {
		return "Maxime Girard (GIRM30058500)  et  Dominic Turgeon (TURD14058501)";
	}

}
