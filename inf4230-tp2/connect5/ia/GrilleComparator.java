package connect5.ia;

import java.util.Comparator;

public class GrilleComparator implements Comparator<Successeur>{

	@Override
	public int compare(Successeur i, Successeur j) {
	    if(JoueurArtificiel.utility(i.getGrille()) > JoueurArtificiel.utility(j.getGrille())) return 1;
	    if(JoueurArtificiel.utility(i.getGrille()) < JoueurArtificiel.utility(j.getGrille())) return -1;
		return 0;
	}
}
